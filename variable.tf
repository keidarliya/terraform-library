variable "region" {
  description = "Azure region: West Europe"
  type        = string
  default     = "West Europe"
}

variable "rg" {
    type= string
    default = "rg-library-dev"
}

variable "vnet" {
  type= string
  default= "vnet-library-dev"
}

variable dbfile{
    type = string
    default = "PostgreSQL_script.sh"
}

variable webfile{
    type = string
    default = "Flask_script1.sh"
}


